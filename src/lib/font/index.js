import font from './font'
import fontFamily from './font-family'
import weight from './weight'

export { fontFamily, font, weight }
